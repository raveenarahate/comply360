//loader
$(window).on("load", function () { $(".status").fadeOut(); $(".preloader").delay(1000).fadeOut("slow"); });


/*  ---------- ON-SCROLL ANIMATION ------------  */
var $animation_elements = $('.animateThis');
var $window = $(window);

function check_if_in_view() {
  var window_height = $window.height();
  var window_top_position = $window.scrollTop();
  var window_bottom_position = (window_top_position + window_height);

  $.each($animation_elements, function () {
    var $element = $(this);
    var element_height = $element.outerHeight();
    var element_top_position = $element.offset().top;
    var element_bottom_position = (element_top_position + element_height);

    if ((element_top_position <= window_bottom_position)) {
      $element.addClass('in-view');
    } else {
      $element.removeClass('in-view');
    }
  });

}

$window.on('scroll resize', check_if_in_view);
$window.trigger('scroll');


/*  ---------- Menu Btn ------------  */
$('.navigBtns').on('click', function () {
  $(this).toggleClass('active');
});



if ($(window).width() < 991) {

  $('.menuBtn').on('click', function () {
    $('.navigHolder').toggleClass('open');
    $('.searchBtn').toggleClass('removehover');
  });

  $('.menuList > li').find('ul').siblings('a').attr('role', 'button').removeAttr('href');
  $('.menuList a[role="button"]').on('click', function (e) {
    $(this).parents('li').siblings().find('ul').slideUp();
    $(this).siblings('ul').slideToggle();
    $(this).parents('li').toggleClass('open');
    $(this).parents('li').siblings().removeClass('open');
  });



}

$('.mobileSearchBtn').click(function () {
  $('.searchBar').slideToggle()
})

//tab
$('.tabStyle li button').click(function () {
  $('.accordianWrp .accordianItem:first').find('.accordianContent').show();
  $('.accordianWrp .accordianItem:first').addClass('active');

})

//Accordian
$('.accordianWrp .accordianItem:first').find('.accordianContent').show();
$('.accordianWrp .accordianItem:first').addClass('active');
$('.accordianHead').click(function () {
  $(this).next('.accordianContent').slideDown();
  $(this).parents('.accordianItem').siblings('.accordianItem').find('.accordianContent').slideUp()

  $(this).parents('.accordianItem').addClass('active');
  $(this).parents('.accordianItem').siblings('.accordianItem').removeClass('active')
})



// Mobile Footer

$('.mobileFootBox .footHead').next('.footList').siblings('a').attr('role', 'button').removeAttr('href');

if ($(window).width() < 1200.99) {

  $('.mobileFootBox .footHead').click(function () {
    $(this).toggleClass('active');
    $(this).parents('.mobileFootWrp').siblings().find('.footHead').removeClass('active')

    $(this).next('.footList').slideToggle();
    $(this).parents('.mobileFootWrp').siblings().find('.footList').slideUp()
  })

}


$(window).scroll(function () {
  if ($(window).scrollTop() > 100) {
    $(".libraryHead").addClass("fixed");
  } else {
    $(".libraryHead").removeClass("fixed");
  }
});



// custom dropdown for select state for checkbox


// document.addEventListener("DOMContentLoaded", function () {
//   var dropdown = document.querySelector(".custom-dropdown");
//   var selectedOption = dropdown.querySelector(".selected-option");
//   var dropdownContent = dropdown.querySelector(".dropdown-content");

//   dropdownContent.addEventListener("click", function (event) {
//     var target = event.target;
//     if (target.tagName === "LI") {
//       var value = target.getAttribute("data-value");
//       var text = target.textContent;
//       var checkbox = document.createElement("input");
//       checkbox.type = "checkbox";
//       target.innerHTML = "";
//       target.appendChild(checkbox);
//       target.insertAdjacentHTML("beforeend", text);
//       dropdownContent.style.display = "none";
//     }
//   });

//   selectedOption.addEventListener("click", function () {
//     dropdownContent.style.display = (dropdownContent.style.display === "block") ? "none" : "block";
//   });
// });




document.addEventListener("DOMContentLoaded", function () {
  var dropdown = document.querySelector(".selectState .custom-dropdown");
  var selectedOption = dropdown.querySelector(".selected-option");
  var dropdownContent = dropdown.querySelector(".dropdown-content");

  // Event listener for clicks outside the dropdown
  document.addEventListener("click", function (event) {
      var isClickInsideDropdown = dropdown.contains(event.target);
      if (!isClickInsideDropdown) {
          dropdownContent.style.display = "none";
      }
  });

  dropdownContent.addEventListener("click", function (event) {
      event.stopPropagation(); // Prevent the click event from reaching the document

      var target = event.target;
      if (target.tagName === "LI") {
          var value = target.getAttribute("data-value");
          var text = target.textContent;
          var checkbox = document.createElement("input");
          checkbox.type = "checkbox";
          target.innerHTML = "";
          target.appendChild(checkbox);
          target.insertAdjacentHTML("beforeend", text);
          dropdownContent.style.display = "none";
      }
  });

  selectedOption.addEventListener("click", function () {
      dropdownContent.style.display = (dropdownContent.style.display === "block") ? "none" : "block";
  });
});





// scroll
if (window.location.hash) {
  var hash = window.location.hash;
console.log($(hash).offset().top)
  if (hash) {
      $('html, body').animate({
          scrollTop: $(hash).offset().top - 300
      }, 1500,);
  }
}