//Accordian
$('.accordion-button').click(function(){
    $(this).toggleClass('active');
    $(this).parent('.accordion-item').siblings('.accordion-item').find('.accordion-button').removeClass('active');
  
    $(this).next('.accordion-body').slideToggle();
    $(this).parent('.accordion-item').siblings('.accordion-item').find('.accordion-body').slideUp();
  })